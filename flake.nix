{
  description = "Call anyrun with the stdin plugin.";

  inputs = {
    anyrun = {
      url = "github:Kirottu/anyrun";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {nixpkgs, ...} @ inputs: let
    system = "x86_64-linux";
    inherit (inputs.anyrun.packages.${system}) anyrun stdin;
    pkgs = import nixpkgs {inherit system;};
  in {
    packages.${system}.default =
      pkgs.writeShellScriptBin "clipboard-anyrun"
      ''
        ${anyrun}/bin/anyrun --plugins ${stdin}/lib/libstdin.so;
      '';
  };
}
